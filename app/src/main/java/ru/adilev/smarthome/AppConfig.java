package ru.adilev.smarthome;

public class AppConfig {
	// Адрес скрипта для подключения
	public static String PROTOCOL = "http://";

	// Адрес скрипта для подключения
	public static String URL_LOGIN = "/SMART/login.php";

	// Адрес скрипта для получения конфигурации системы
	public static String URL_GET_CONFIG = "/SMART/getConfig.php";

	// Адрес скрипта, в который отправляются данные после изменения параметров устройства
	public static String URL_SET_VALUE = "/SMART/setValue.php";

	// Адрес скрипта для получения данных о состоянии устройств из истории
	public static String URL_GET_HISTORY_DATA = "/SMART/getGraphData.php";
}
