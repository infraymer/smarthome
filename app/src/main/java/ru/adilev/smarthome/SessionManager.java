package ru.adilev.smarthome;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionManager {
	// LogCat tag
//	private static String TAG = SessionManager.class.getSimpleName();

	// Shared Preferences
	private SharedPreferences pref;

	private Editor editor;
	private Context mContext;

	// Shared pref mode
	private int PRIVATE_MODE = 0;

	// Shared preferences file name
	private static final String PREF_NAME = "SmartHomeLogin";
	private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
	private static final String KEY_USERNAME = "username";
	private static final String KEY_PASSWORD = "password";

	public SessionManager(Context context) {
		this.mContext = context;
		pref = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	public void setLogin(boolean isLoggedIn) {

		editor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn);

		// commit changes
		editor.commit();

//		Log.d(TAG, "User login session modified!");
	}
	
	public boolean isLoggedIn(){
		return pref.getBoolean(KEY_IS_LOGGED_IN, false);
	}

	public void setUsername(String username){
		editor.putString(KEY_USERNAME, username);
		editor.commit();
	}

	public void setPassword(String password){
		editor.putString(KEY_PASSWORD, password);
		editor.commit();
	}

	public String getUsername(){
		return pref.getString(KEY_USERNAME, "");
	}

	public String getPassword(){
		return pref.getString(KEY_PASSWORD, "");
	}
}
