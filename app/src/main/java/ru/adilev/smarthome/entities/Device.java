package ru.adilev.smarthome.entities;

import java.util.ArrayList;
import java.util.List;

public class Device {

    private String name;
    private int idDopAddr;
    private List<Function> functions = new ArrayList<Function>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdDopAddr() {
        return idDopAddr;
    }

    public void setIdDopAddr(int idDopAddr) {
        this.idDopAddr = idDopAddr;
    }

    public List<Function> getFunctions() {
        return functions;
    }

    public void setFunctions(List<Function> functions) {
        this.functions = functions;
    }
}

