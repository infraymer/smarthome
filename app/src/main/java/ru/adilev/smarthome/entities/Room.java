package ru.adilev.smarthome.entities;

import java.util.ArrayList;
import java.util.List;

public class Room {

    private String name;
    private List<Device> devices = new ArrayList<Device>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

}

