package ru.adilev.smarthome.entities;

import java.util.ArrayList;
import java.util.List;

public class Category {

    private String name;
    private List<Room> rooms = new ArrayList<Room>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

}
