package ru.adilev.smarthome.entities;

public class Function {

	private String name;
    private int idFunc;
    private int value;
    private int writeEnable;
    private int valMin;
    private int valMax;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdFunc() {
        return idFunc;
    }

    public void setIdFunc(int idFunc) {
        this.idFunc = idFunc;
    }
    
    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getWriteEnable() {
        return writeEnable;
    }

    public void setWriteEnable(int writeEnable) {
        this.writeEnable = writeEnable;
    }

    public int getValMin() {
        return valMin;
    }

    public void setValMin(int valMin) {
        this.valMin = valMin;
    }

    public int getValMax() {
        return valMax;
    }

    public void setValMax(int valMax) {
        this.valMax = valMax;
    }
}
