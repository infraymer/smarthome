package ru.adilev.smarthome.entities;

import java.util.List;

public class DeviceInfo {

    private static List<State> states = null;

    public static List<State> getStates() {
        return states;
    }

    public void setStates(List<State> states) {
        this.states = states;
    }
}
