package ru.adilev.smarthome.entities;

import java.util.List;

public class GraphData {

    private static List<Point> points = null;
  
    public static List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }

}
