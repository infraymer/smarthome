package ru.adilev.smarthome.entities;

import java.util.ArrayList;
import java.util.List;

public class HomeInfo {

    private static List<Category> categories = new ArrayList<Category>();

    public static List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    /*public void print() {
        System.out.println();
        System.out.println( "info: ");
        for(Category next :  getCategories()){
        	System.out.println( next.getName() );
        }
    }*/
}
