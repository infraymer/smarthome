package ru.adilev.smarthome.entities;

public class Point {

    private String x;
    private float y;
  
    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

}
