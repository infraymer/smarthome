package ru.adilev.smarthome.entities;

public class State {

    private int idDopAddr;
    private int idFunc;
    private int value;

    public int getIdDopAddr() {
        return idDopAddr;
    }

    public void setIdDopAddr(int idDopAddr) {
        this.idDopAddr = idDopAddr;
    }

    public int getIdFunc() {
        return idFunc;
    }

    public void setIdFunc(int idFunc) {
        this.idFunc = idFunc;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}

