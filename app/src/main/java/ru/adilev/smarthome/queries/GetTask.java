package ru.adilev.smarthome.queries;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;

import ru.adilev.smarthome.R;

public class GetTask extends AsyncTask<String, Void, Response> {
    Context mContext;

    public GetTask(Context context){
        super();
        this.mContext = context;
    }

    protected void onPreExecute() {
        super.onPreExecute();
        Toast.makeText(mContext, mContext.getResources().getString(R.string.executing_request), Toast.LENGTH_SHORT).show();
    }

    protected Response doInBackground(String... urls) {
        String result = "", textError = "";
        HttpClient client = new DefaultHttpClient();

        try {
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit

            HttpGet httpGet = new HttpGet(urls[0]);

            HttpEntity httpEntity = client.execute(httpGet).getEntity();

            result = EntityUtils.toString(httpEntity);
        }
        catch (Throwable t) {
            textError = t.toString();
            return new Response(false, result, textError);
        }
        return new Response(true, result, textError);
    }

    protected void onPostExecute(Response result) {
        if(result.status){
            Toast.makeText(mContext, mContext.getResources().getString(R.string.data_is_load),Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(mContext, mContext.getResources().getString(R.string.bad_request)
                    + result.textError,Toast.LENGTH_LONG).show();
        }
    }
}