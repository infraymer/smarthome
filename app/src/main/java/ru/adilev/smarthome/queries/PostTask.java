package ru.adilev.smarthome.queries;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import ru.adilev.smarthome.R;

public class PostTask extends AsyncTask<String, Void, Response> {
    Context mContext;
    String[] mKeys, mValues;

    public PostTask(Context context, String[] keys, String[] values){
        super();
        this.mContext = context;
        this.mKeys = keys;
        this.mValues = values;
    }

    protected void onPreExecute() {
        super.onPreExecute();
        Toast.makeText(mContext, mContext.getResources().getString(R.string.executing_request), Toast.LENGTH_SHORT).show();
    }

    protected Response doInBackground(String... urls) {
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        JSONObject json = new JSONObject();

        String result = "", textError = "";

        try {
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit

            HttpPost httpPost = new HttpPost(urls[0]);

            for(int i=0; i<mKeys.length; i++){
                json.put(mKeys[i], mValues[i]);
            }

            StringEntity httpEntity  = new StringEntity( json.toString(), HTTP.UTF_8);
            httpEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            httpPost.setEntity(httpEntity);
            response = client.execute(httpPost);

            result = EntityUtils.toString(response.getEntity());
        }
        catch (Throwable t) {
            textError = t.toString();
            return new Response(false, result, textError);
        }
        return new Response(true, result, textError);
    }

    protected void onPostExecute(Response result) {
        if(result.status){
//            Toast.makeText(mContext,"Данные отправлены!",Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(mContext, mContext.getResources().getString(R.string.bad_request)
                    + result.textError,Toast.LENGTH_LONG).show();
        }
    }
}