package ru.adilev.smarthome.queries;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import ru.adilev.smarthome.R;
import ru.adilev.smarthome.entities.DeviceInfo;
import ru.adilev.smarthome.views.MainActivity;

public class SocketTask extends AsyncTask<Void,String,Boolean> {

    String response;
    Context mContext;
    Socket socket;
    String host;
    int port;
    Boolean isCheckedConnect;
    Boolean isConnected;
    Boolean isClosed;

    OnSocketTaskComplete listener;

    // Interface to be implemented by calling activity
    public  interface OnSocketTaskComplete {
        public void onTaskDone();
    }

    public SocketTask(Context context, OnSocketTaskComplete listener, String host, int port) {
        super();
        this.mContext = context;
        this.listener = listener;
        this.host = host;
        this.port = port;
        response = "";
        socket = null;
        isConnected = false;
        isCheckedConnect = false;
        isClosed = false;
    }

    protected void onPreExecute() {
        super.onPreExecute();
    }

    protected Boolean doInBackground(Void... params) {
        try {
            socket = new Socket(host, port);
//                ByteArrayOutputStream byteArrayOutputStream =
//                        new ByteArrayOutputStream(1024);
            byte[] buffer = new byte[16384];
            InputStream inputStream = socket.getInputStream();
            int bytesRead;

            if(socket.isConnected()){
                isConnected = true;
                isCheckedConnect = true;
                publishProgress("");
            }
            else {
                isConnected = false;
                isCheckedConnect = true;
                publishProgress("");
                return false;
            }

            while((bytesRead = inputStream.read(buffer)) != -1){
//                    publishProgress(bytesRead);
//                    bytesRead = inputStream.read(buffer);

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(16384);
                byteArrayOutputStream.write(buffer, 0, bytesRead);
                response = byteArrayOutputStream.toString("UTF-8");
                publishProgress(response);

                if (isCancelled())
                    return false;
            }
            inputStream.close();
//                socket.close();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
            response = "UnknownHostException: " + e.toString();
        } catch (IOException e) {
            e.printStackTrace();
            response = "IOException: " + e.toString();
        }
        catch (Throwable t) {
            response = "Throwable: " + t.toString();
        }
        finally{
            if(socket != null){
                try {
                    socket.close();
                    isClosed = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        try{
            if(isCheckedConnect){
                if(isConnected){
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.socket_is_connected), Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.socket_is_not_connected), Toast.LENGTH_SHORT).show();
                }
                isCheckedConnect = false;
            }
            else if(values.length > 0){  // Запить данных в HomeInfo
                String socketStr = values[0].toString();
                int pos = socketStr.indexOf("{");
//                Toast.makeText(mContext, "MES: "+socketStr, Toast.LENGTH_LONG).show();
                if(pos != -1){
                    socketStr = socketStr.substring(pos);
                    //для отладки
                    if(MainActivity.debug) {
                        Toast.makeText(mContext, "MES: "+socketStr, Toast.LENGTH_LONG).show();
                    }
                    // convert from json
                    JSON.parseObject(socketStr, DeviceInfo.class);

                    listener.onTaskDone();
                   // create interface
                }
            }
        }
        catch (Throwable t) {
            Toast.makeText(mContext,"Error of socket message: "+t.toString(), Toast.LENGTH_LONG).show();
        }
    }

    protected void onPostExecute(Boolean result) {
        if(isClosed){
            Toast.makeText(mContext, mContext.getResources().getString(R.string.socket_is_closed), Toast.LENGTH_LONG).show();
            isClosed = false;
        }
        else {
            Toast.makeText(mContext, response, Toast.LENGTH_LONG).show();
        }
//        super.onPostExecute(result);
    }
}