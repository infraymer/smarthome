package ru.adilev.smarthome.queries;

public class Response {
    public boolean status;
    public String textResponse;
    public String textError;
    Response(boolean status, String textResponse, String textError) {
        this.status = status;
        this.textResponse = textResponse;
        this.textError = textError;
    }
}