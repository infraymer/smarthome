package ru.adilev.smarthome;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.Calendar;

public class Utils {

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager conManager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conManager.getActiveNetworkInfo();

        return !(netInfo == null || !netInfo.isConnected());
    }
    public static String beautyHexString(String hexString) {
        if (hexString.length() < 2) {
            return "0".concat(hexString);
        } else {
            return hexString;
        }
    }
    public static String dateString(Calendar c) {
        return c.get(Calendar.DAY_OF_MONTH)+"/"+(c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.YEAR);
    }
    public static String timeString(Calendar c) {
        return c.get(Calendar.HOUR_OF_DAY) +":"+ c.get(Calendar.MINUTE);
    }
//    public static String dateAndTimeString(Calendar c) {
//        return dateString(c)+" "+timeString(c);
//    }
}

