package ru.adilev.smarthome.views;

/**
 * Interface with callback methods for ColorPicker dialog.
 */
public interface OnColorPickerListener {
    void onCancel(ColorPickerDialogFragment dialogFragment);

    void onOk(ColorPickerDialogFragment dialogFragment, int color);
}
