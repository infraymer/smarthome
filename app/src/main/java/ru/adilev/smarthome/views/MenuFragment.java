
package ru.adilev.smarthome.views;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.adilev.smarthome.R;
import ru.adilev.smarthome.entities.HomeInfo;
import ru.adilev.smarthome.entities.Room;

public class MenuFragment extends Fragment {
    final static String ARG_POSITION = "position";
    private int mCurrentPosition = -1;
    private int mCurrentPositionTab = -1;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
        }

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.menu_view, container, false);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        Bundle args = getArguments();
        if (args != null) {
            // Set fragment based on argument passed in
            updateMenuView(args.getInt(ARG_POSITION));
        } else if (mCurrentPosition != -1) {
            // Set fragment based on saved instance state defined during onCreateView
            updateMenuView(mCurrentPosition);
        }
    }

    @Override
    public void onResume() {
        //do something
        super.onResume();
    }

    public void updateMenuView(int position) {

        viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);

        // Initializing the viewpager with fragments
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        // ---------------------------------------------
//        MenuFragment newMenuFragment = (MenuFragment) getSupportFragmentManager().findFragmentById(R.id.frame_container);
//
//        // Update fragment
//        if (newMenuFragment != null && mCurrentPosition != position) {
//            newMenuFragment.updateMenuView(position);
//            Toast.makeText(this,"updateMenuView "+position, Toast.LENGTH_SHORT).show();
//            // Загрузка фрагмента
//            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//            transaction.replace(R.id.frame_container, newMenuFragment);
//            transaction.addToBackStack(null);
//            transaction.commit();
//
//            mCurrentPosition = position;
//        } else {    // Create fragment
//            Toast.makeText(this,"new", Toast.LENGTH_SHORT).show();
//            newMenuFragment = new MenuFragment();
//            Bundle args = new Bundle();
//            args.putInt(MenuFragment.ARG_POSITION, position);
//            newMenuFragment.setArguments(args);
//            // Загрузка фрагмента
//            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//            transaction.replace(R.id.frame_container, newMenuFragment);
//            transaction.addToBackStack(null);
//            transaction.commit();
//        }
        // ---------------------------------------------

        int posTab=0;
        for(Room next : HomeInfo.getCategories().get(position).getRooms()){   // addFragment (arg, tabHeader)

//            if (newFragment != null) {
//                newFragment.updateTabView(position, posTab);
//
//               // Toast.makeText(getActivity(),"updateTabView", Toast.LENGTH_SHORT).show();
//            } else {
            TabFragment newFragment = new TabFragment();
            Bundle args = new Bundle();
            args.putInt(TabFragment.ARG_POSITION, position);
            args.putInt(TabFragment.ARG_POSITION_TAB, posTab);
            newFragment.setArguments(args);
            adapter.addFragment(newFragment, next.getName());
//            }

            //adapter.addFragment(TabFragment.newInstance(position, i), next.getName());
            posTab++;
        }

        viewPager.setAdapter(adapter);
        tabLayout = (TabLayout) getActivity().findViewById(R.id.tab_layout);

        //adding viewpager to the tablayout
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(ContextCompat.getColorStateList(getActivity(), R.color.tab_selector));
        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
        if(mCurrentPositionTab != -1){
            viewPager.setCurrentItem(mCurrentPositionTab);
        }
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mCurrentPositionTab = tab.getPosition();
//                Toast.makeText(getActivity(),"tab.getPosition() "+mCurrentPositionTab, Toast.LENGTH_SHORT).show();
                viewPager.setCurrentItem(mCurrentPositionTab);
            }

            public void onTabUnselected(TabLayout.Tab tab) {}

            public void onTabReselected(TabLayout.Tab tab) {}
        });
        //-----------------------
        mCurrentPosition = position;
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the current menu selection in case we need to recreate the fragment
        outState.putInt(ARG_POSITION, mCurrentPosition);
    }

}