package ru.adilev.smarthome.views;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import ru.adilev.smarthome.AppConfig;
import ru.adilev.smarthome.R;
import ru.adilev.smarthome.SessionManager;
import ru.adilev.smarthome.Utils;
import ru.adilev.smarthome.queries.PostTask;
import ru.adilev.smarthome.queries.Response;

public class LoginActivity extends AppCompatActivity {

    private Button btnLogin;
    private Button btnLinkToDemo;
    private EditText inputUsername;
    private EditText inputPassword;
//    private ProgressDialog pDialog;
    private SessionManager session;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

//        try{
        inputUsername = (EditText) findViewById(R.id.username);
        inputPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLinkToDemo = (Button) findViewById(R.id.btnLinkToDemo);

        // Session manager
        session = new SessionManager(getApplicationContext());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity

            // Вытаскиваем данные и логинимся
            String username = session.getUsername();
            String password = session.getPassword();

            checkLogin(username, password);
        }

        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MainActivity.demo = false;

                String username = inputUsername.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                checkLogin(username, password);
            }
        });

        // Link to Demo
        btnLinkToDemo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MainActivity.demo = true;

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

//        }
//        catch (Throwable t) {
//            Toast.makeText(this,"Log Error: "+t.toString(), Toast.LENGTH_LONG).show();
//        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        //return super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.action_settings:
                intent = new Intent(this, PrefActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    // проверка корректности заполнения полей
    private void checkLogin(String username, String password) {
        // Check for empty data in the form
        if (!username.isEmpty() && !password.isEmpty()) {
            // login user
            String mKeys[] = {"username","password"},
                    mValues[] = {username, password};
            login(mKeys, mValues);
        } else {
            // Prompt user to enter credentials
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.empty_fields), Toast.LENGTH_LONG).show();
        }
    }

    // выполнение запроса для входа
    private void login(String[] keys, String[] values){
        boolean isNetworkAvailable = Utils.isNetworkAvailable(getApplicationContext());
        if (!isNetworkAvailable) {
            Snackbar.make(this.findViewById(android.R.id.content), getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } else {
            // получаем SharedPreferences, которое работает с файлом настроек
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
            String address = sp.getString("address", "");
            String URL = AppConfig.PROTOCOL + address + AppConfig.URL_LOGIN;

            PostTask task = new PostTask(this, keys, values);
            Response res = null;
            try {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                    res = task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL).get();
                else
                    res = task.execute(URL).get();
            } catch (InterruptedException e) {
            } catch (ExecutionException e) {}

            if(res.status){
                try {
                    JSONObject jObj = new JSONObject(res.textResponse);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session
                        session.setLogin(true);
                        session.setUsername(values[0]);
                        session.setPassword(values[1]);

                        // Launch main activity
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }else {
                        Toast.makeText(this, getResources().getString(R.string.wrong_data),Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(this, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

//    private void showDialog() {
//        if (!pDialog.isShowing())
//            pDialog.show();
//    }
//
//    private void hideDialog() {
//        if (pDialog.isShowing())
//            pDialog.dismiss();
//    }
}
