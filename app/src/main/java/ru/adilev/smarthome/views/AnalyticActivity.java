package ru.adilev.smarthome.views;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

import ru.adilev.smarthome.AppConfig;
import ru.adilev.smarthome.R;
import ru.adilev.smarthome.Utils;
import ru.adilev.smarthome.entities.Device;
import ru.adilev.smarthome.entities.Function;
import ru.adilev.smarthome.entities.GraphData;
import ru.adilev.smarthome.entities.HomeInfo;
import ru.adilev.smarthome.entities.Point;
import ru.adilev.smarthome.queries.PostTask;
import ru.adilev.smarthome.queries.Response;


public class AnalyticActivity extends AppCompatActivity implements View.OnClickListener{

    private int DIALOG_TIME = 0;
    private int DIALOG_DATE = 1;
    private int idDialogInCategoty = 0;
    private Toolbar mToolbar;
    private TextView tvBeginDate, tvBeginTime, tvEndDate, tvEndTime;
    private LinearLayout linLayout;
    private LayoutInflater ltInflater;
//    int i=0;
//    private Context mContext;

    private int id_dop_addr = -1;
    private int id_func = -1;
    private String name_func = "";
    private String funcName[] = {};
    private int funcId[] = {};

    private String jsonDemo = "{\"points\":[{\"x\":\"19\\/11\\/2016 20:56:47\",\"y\":10},{\"x\":\"23\\/04\\/2017 16:38:27\",\"y\":70},{\"x\":\"23\\/04\\/2017 16:53:00\",\"y\":44},{\"x\":\"23\\/04\\/2017 16:53:18\",\"y\":74},{\"x\":\"23\\/04\\/2017 17:02:25\",\"y\":82},{\"x\":\"23\\/04\\/2017 17:22:34\",\"y\":29},{\"x\":\"23\\/04\\/2017 17:34:19\",\"y\":63},{\"x\":\"23\\/04\\/2017 17:34:45\",\"y\":91},{\"x\":\"23\\/04\\/2017 17:35:32\",\"y\":29},{\"x\":\"23\\/04\\/2017 17:37:56\",\"y\":36},{\"x\":\"23\\/04\\/2017 17:38:06\",\"y\":79},{\"x\":\"23\\/04\\/2017 17:38:21\",\"y\":87},{\"x\":\"23\\/04\\/2017 17:43:01\",\"y\":29}]}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            setContentView(R.layout.activity_analytic);
            mToolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(mToolbar);

            Intent intent = getIntent();
            id_dop_addr = intent.getIntExtra("id_dop_addr", 0);
            int mCurrentPosition = intent.getIntExtra("mCurrentPosition", 0);
            int mCurrentPositionTab = intent.getIntExtra("mCurrentPositionTab", 0);

            for(Device dev_next : HomeInfo.getCategories().get(mCurrentPosition).getRooms().get(mCurrentPositionTab).getDevices()) {
                if(dev_next.getIdDopAddr() == id_dop_addr){
                    int n = dev_next.getFunctions().size();
                    funcName = new String[n];
                    funcId = new int[n];
                    for(int i=0; i<n; i++) {
                        Function func_next = dev_next.getFunctions().get(i);
                        funcName[i] = func_next.getName();
                        funcId[i] = func_next.getIdFunc();
                    }
                }
            }

            // адаптер
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, funcName);
            adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
            final Spinner spinner = (Spinner) findViewById(R.id.spinner);
            spinner.setAdapter(adapter);
            // заголовок
            spinner.setPrompt(getResources().getString(R.string.choice_function));
            // выделяем элемент
            spinner.setSelection(0);
            // устанавливаем обработчик нажатия
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // показываем позиция нажатого элемента
                id_func = funcId[position];
                name_func = funcName[position];

                Toast.makeText(getBaseContext(), "Position = " + position +
                        "\nFunc = "+id_func, Toast.LENGTH_SHORT).show();
                }
                @Override
                public void onNothingSelected(AdapterView<?> arg0) {}
            });

            initView();
        }
        catch (Throwable t) {
            Toast.makeText(this,"Error analytic view: "+t.toString(), Toast.LENGTH_SHORT).show();
        }
//        initializeGraph();
//        mContext = getApplicationContext();
    }

    // инициализация
    private void initView() {
        tvBeginDate = (TextView) findViewById(R.id.beginDate);
        tvBeginTime = (TextView) findViewById(R.id.beginTime);
        tvEndDate = (TextView) findViewById(R.id.endDate);
        tvEndTime = (TextView) findViewById(R.id.endTime);
        Button btnGetGraph = (Button) findViewById(R.id.btnGetGraph);

        tvBeginDate.setOnClickListener(this);
        tvBeginTime.setOnClickListener(this);
        tvEndDate.setOnClickListener(this);
        tvEndTime.setOnClickListener(this);
        btnGetGraph.setOnClickListener(this);

        Calendar calendar = Calendar.getInstance();
        tvBeginDate.setText(Utils.dateString(calendar));
        tvBeginTime.setText(Utils.timeString(calendar));
        tvEndDate.setText(Utils.dateString(calendar));
        tvEndTime.setText(Utils.timeString(calendar));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.beginDate:
                showDialog(DIALOG_DATE);
                idDialogInCategoty = 0;
                break;
            case R.id.beginTime:
                showDialog(DIALOG_TIME);
                idDialogInCategoty = 0;
                break;
            case R.id.endDate:
                showDialog(DIALOG_DATE);
                idDialogInCategoty = 1;
                break;
            case R.id.endTime:
                showDialog(DIALOG_TIME);
                idDialogInCategoty = 1;
                break;
            case R.id.btnGetGraph:
                String date_begin = tvBeginDate.getText().toString() + " " + tvBeginTime.getText().toString();
                String date_end = tvEndDate.getText().toString() + " " + tvEndTime.getText().toString();

                // Check date_begin <= date_end
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                try {
                    cal.setTime(sdf.parse(date_begin));
                    long time_begin = cal.getTimeInMillis();
                    cal.setTime(sdf.parse(date_end));
                    long time_end = cal.getTimeInMillis();

                    if(time_begin > time_end){
                        Toast.makeText(this, getResources().getString(R.string.wrong_time_period), Toast.LENGTH_SHORT).show();
                    }
                    else {
                        String mKeys[] = {"id_dop_addr","id_func","date_begin", "date_end"},
                                mValues[] = {String.valueOf(id_dop_addr), String.valueOf(id_func), date_begin, date_end};

                        getGraphData(mKeys, mValues);
                    }

                }
                catch (Throwable t) {
                    Toast.makeText(this,"Error get graph: "+t.toString(), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    // получение данных для графика
    private void getGraphData(String[] keys, String[] values){
        Boolean demo = MainActivity.demo;
        if(demo){
            Snackbar.make(this.findViewById(android.R.id.content), getResources().getString(R.string.local_loading), Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
            // convert from json
            JSON.parseObject(jsonDemo, GraphData.class);
            getGraph();
        }
        else{
            boolean isNetworkAvailable = Utils.isNetworkAvailable(getApplicationContext());
            if (!isNetworkAvailable) {
                Snackbar.make(this.findViewById(android.R.id.content), getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            } else {
                PostTask task = new PostTask(this, keys, values);
                Response res = null;
                String URL = AppConfig.PROTOCOL + MainActivity.address + AppConfig.URL_GET_HISTORY_DATA;
                try {
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                        res = task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL).get();
                    else
                        res = task.execute(URL).get();
//                res = task.execute(URL2).get();
                } catch (InterruptedException e) {
                } catch (ExecutionException e) {}

                if(res.status){
//                Toast.makeText(this,"Ответ: " + res.textResponse,Toast.LENGTH_SHORT).show();
                    String jsonString = res.textResponse;
                    // convert from json
                    JSON.parseObject(jsonString, GraphData.class);
                    getGraph();
                }
            }
        }
    }

    // построение графика
    private void getGraph() {
        linLayout = (LinearLayout) findViewById(R.id.linLayout);
        ltInflater = getLayoutInflater();
        linLayout.removeAllViews();

        // Начало и конец временного промежутка
        String beginText="",endText="";
        // Заполняем массив точек
        int n = GraphData.getPoints().size();
        if(n == 0){
            Toast.makeText(this, getResources().getString(R.string.no_data_in_history), Toast.LENGTH_LONG).show();
            return;
        }
        DataPoint[] Points = new DataPoint[n];
//        for(int i=0; i<n; i++){
//            Points[i] =  new DataPoint(i, 30-i*2);
//        }

        int average = 0;
        for(int i=0; i<n; i++){
            Point next =  GraphData.getPoints().get(i);
            if(i == 0){ beginText = next.getX(); }
            Points[i] =  new DataPoint(i, next.getY());
            endText = next.getX();

            Points[i] =  new DataPoint(i, next.getY());
            average += next.getY();
        }
        average /= n;
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(Points);

        series.setDrawBackground(true);

        //Связываемся с элементами разметки
        View item = ltInflater.inflate(R.layout.graph_view, linLayout, false);
        TextView graphTitle = (TextView) item.findViewById(R.id.graphTitle);
        // Set title
        graphTitle.setText(name_func);
        // Set graph
        GraphView graph = (GraphView) item.findViewById(R.id.graph);
        // Set max field
        TextView maxValue = (TextView) item.findViewById(R.id.maxValue);
        maxValue.setText(String.valueOf(series.getHighestValueY()));
        // Set min field
        TextView minValue = (TextView) item.findViewById(R.id.minValue);
        minValue.setText(String.valueOf(series.getLowestValueY()));
        // Set average field
        TextView averageValue = (TextView) item.findViewById(R.id.averageValue);
        averageValue.setText(String.valueOf(average));
        linLayout.addView(item);
        // Set graph color
        series.setBackgroundColor(getResources().getColor(R.color.colorUnderGraph));
        series.setColor(getResources().getColor(R.color.colorGraph));
        graph.addSeries(series);

        // customize viewport
        Viewport viewport = graph.getViewport();
        viewport.setYAxisBoundsManual(true);
        viewport.setMinY(series.getLowestValueY());
        viewport.setMaxY(series.getHighestValueY());

//        viewport.setXAxisBoundsManual(true);
//        viewport.setMinX(series.getLowestValueX());
//        viewport.setMaxX(series.getHighestValueX());

        // activate horizontal zooming and scrolling
        viewport.setScalable(true);

        // activate horizontal scrolling
//        viewport.setScrollable(true);

        // Set grid style
//        graph.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.NONE);
        graph.getGridLabelRenderer().setGridColor(Color.WHITE);
        graph.getGridLabelRenderer().setHorizontalAxisTitleColor(Color.WHITE);
        graph.getGridLabelRenderer().setHorizontalAxisTitle(beginText + "\t\t\t" + endText);
//        graph.setBackgroundColor(getResources().getColor(R.color.colorBackGraph));
//        graph.getGridLabelRenderer().setNumVerticalLabels(GraphData.getPoints().size());
        graph.getGridLabelRenderer().setNumHorizontalLabels(3);
        // change color of labels
        graph.getGridLabelRenderer().setVerticalLabelsColor(Color.WHITE);
        graph.getGridLabelRenderer().setHorizontalLabelsColor(getResources().getColor(R.color.colorBackGraph));


        // use static labels for horizontal label
//        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
//        staticLabelsFormatter.setHorizontalLabels(new String[] {beginText,endText});
//        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);

//        graph.getGridLabelRenderer().setHumanRounding(false);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Deprecated
    protected Dialog onCreateDialog(int id) {
        Calendar c = Calendar.getInstance();
        if (id == DIALOG_TIME) {
            TimePickerDialog timePickerDialog = new TimePickerDialog(this, timePickerListener,
                    c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true);
            return timePickerDialog;
        }
        if (id == DIALOG_DATE) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, datePickerListener,
                    c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
            return datePickerDialog;
        }

        return super.onCreateDialog(id);
    }

    // установка слушателя для диалогового окна времени
    TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            if(idDialogInCategoty==0){
                ((TextView) findViewById(R.id.beginTime))
                        .setText(hourOfDay + ":" + minute);
            }
            else {
                ((TextView) findViewById(R.id.endTime))
                        .setText(hourOfDay + ":" + minute);
            }
        }
    };
    // установка слушателя для диалогового окна даты
    DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            if(idDialogInCategoty==0){
                ((TextView) findViewById(R.id.beginDate))
                        .setText(dayOfMonth + "/" + (monthOfYear+1) + "/" + year);
            }
            else {
                ((TextView) findViewById(R.id.endDate))
                        .setText(dayOfMonth + "/" + (monthOfYear+1) + "/" + year);
            }
        }
    };
}