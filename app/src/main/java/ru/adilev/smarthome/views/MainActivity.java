package ru.adilev.smarthome.views;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import ru.adilev.smarthome.AppConfig;
import ru.adilev.smarthome.R;
import ru.adilev.smarthome.SessionManager;
import ru.adilev.smarthome.Utils;
import ru.adilev.smarthome.entities.Category;
import ru.adilev.smarthome.entities.Device;
import ru.adilev.smarthome.entities.DeviceInfo;
import ru.adilev.smarthome.entities.Function;
import ru.adilev.smarthome.entities.HomeInfo;
import ru.adilev.smarthome.entities.Room;
import ru.adilev.smarthome.entities.State;
import ru.adilev.smarthome.queries.GetTask;
import ru.adilev.smarthome.queries.PostTask;
import ru.adilev.smarthome.queries.Response;
import ru.adilev.smarthome.queries.SocketTask;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener, SocketTask.OnSocketTaskComplete{

    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListView mLvDrawerMenu;
    private DrawerMenuItemAdapter mDrawerMenuAdapter;

    private int mCurrentPosition = -1;
//    public int mCurrentPositionTab = -1;

    public static Boolean demo = false;
    public static Boolean debug = false;
    public static Boolean withoutСonfirmation = false;
    public static String address;
    private String URL1="", URL2="";
    private String jsonString = "";
    private String jsonDemo = "{\"categories\":[{\"name\":\"\u041e\u0441\u0432\u0435\u0449\u0435\u043d\u0438\u0435\",\"rooms\":[{\"name\":\"\u0433\u043e\u0441\u0442\u0438\u043d\u0430\u044f\",\"devices\":[{\"name\":\"RGB\",\"id_dop_addr\":\"35\",\"functions\":[{\"name\":\"\u0432\u043a\u043b\\/\u0432\u044b\u043a\u043b \u0443\u0441\u0442\u0440\u043e\u0439\u0441\u0442\u0432\u043e\",\"id_func\":2,\"value\":1,\"write_enable\":1,\"val_min\":0,\"val_max\":1},{\"name\":\"RGB\",\"id_func\":11,\"value\":9999,\"write_enable\":1,\"val_min\":0,\"val_max\":32768}]}]},{\"name\":\"\u0441\u043f\u0430\u043b\u044c\u043d\u044f\",\"devices\":[{\"name\":\"\u041b\u0430\u043c\u043f\u0430\",\"id_dop_addr\":\"34\",\"functions\":[{\"name\":\"\u0432\u043a\u043b\\/\u0432\u044b\u043a\u043b \u0443\u0441\u0442\u0440\u043e\u0439\u0441\u0442\u0432\u043e\",\"id_func\":2,\"value\":1,\"write_enable\":1,\"val_min\":0,\"val_max\":1}]},{\"name\":\"\u041b\u0430\u043c\u043f\u0430\",\"id_dop_addr\":\"33\",\"functions\":[{\"name\":\"\u0432\u043a\u043b\\/\u0432\u044b\u043a\u043b \u0443\u0441\u0442\u0440\u043e\u0439\u0441\u0442\u0432\u043e\",\"id_func\":2,\"value\":0,\"write_enable\":1,\"val_min\":0,\"val_max\":1},{\"name\":\"RGB\",\"id_func\":11,\"value\":9999,\"write_enable\":1,\"val_min\":0,\"val_max\":32768}]}]}]},{\"name\":\"\u041e\u0442\u043e\u043f\u043b\u0435\u043d\u0438\u0435\",\"rooms\":[{\"name\":\"\u0432\u0430\u043d\u043d\u0430\",\"devices\":[{\"name\":\"\u041d\u0430\u0433\u0440\u0435\u0432\u0430\u0442\u0435\u043b\u044c\",\"id_dop_addr\":\"31\",\"functions\":[{\"name\":\"\u0432\u043a\u043b\\/\u0432\u044b\u043a\u043b \u0443\u0441\u0442\u0440\u043e\u0439\u0441\u0442\u0432\u043e\",\"id_func\":2,\"value\":0,\"write_enable\":1,\"val_min\":0,\"val_max\":1},{\"name\":\"\u0428\u0418\u041c\",\"id_func\":16,\"value\":13,\"write_enable\":1,\"val_min\":0,\"val_max\":100}]},{\"name\":\"\u0422\u0435\u0440\u043c\u043e\u043c\u0435\u0442\u0440\",\"id_dop_addr\":\"27\",\"functions\":[{\"name\":\"\u0432\u043a\u043b\\/\u0432\u044b\u043a\u043b \u0443\u0441\u0442\u0440\u043e\u0439\u0441\u0442\u0432\u043e\",\"id_func\":2,\"value\":1,\"write_enable\":1,\"val_min\":0,\"val_max\":1},{\"name\":\"\u0442\u0435\u0440\u043c\u043e\u0440\u0435\u0433\u0443\u043b\u044f\u0442\u043e\u0440_\u0443\u0441\u0442\u0430\u0432\u043a\u0430\",\"id_func\":12,\"value\":39,\"write_enable\":1,\"val_min\":0,\"val_max\":40},{\"name\":\"\u0442\u0435\u043a\u0443\u0449\u0430\u044f \u0442\u0435\u043c\u043f\u0435\u0440\u0430\u0442\u0443\u0440\u0430\",\"id_func\":13,\"value\":23,\"write_enable\":0,\"val_min\":-40,\"val_max\":100}]}]},{\"name\":\"\u0433\u043e\u0441\u0442\u0438\u043d\u0430\u044f\",\"devices\":[{\"name\":\"\u041d\u0430\u0433\u0440\u0435\u0432\u0430\u0442\u0435\u043b\u044c\",\"id_dop_addr\":\"30\",\"functions\":[{\"name\":\"\u0432\u043a\u043b\\/\u0432\u044b\u043a\u043b \u0443\u0441\u0442\u0440\u043e\u0439\u0441\u0442\u0432\u043e\",\"id_func\":2,\"value\":1,\"write_enable\":1,\"val_min\":0,\"val_max\":1},{\"name\":\"\u0428\u0418\u041c\",\"id_func\":16,\"value\":12,\"write_enable\":1,\"val_min\":0,\"val_max\":100}]},{\"name\":\"\u0422\u0435\u0440\u043c\u043e\u043c\u0435\u0442\u0440\",\"id_dop_addr\":\"26\",\"functions\":[{\"name\":\"\u0432\u043a\u043b\\/\u0432\u044b\u043a\u043b \u0443\u0441\u0442\u0440\u043e\u0439\u0441\u0442\u0432\u043e\",\"id_func\":2,\"value\":1,\"write_enable\":1,\"val_min\":0,\"val_max\":1},{\"name\":\"\u0442\u0435\u0440\u043c\u043e\u0440\u0435\u0433\u0443\u043b\u044f\u0442\u043e\u0440_\u0443\u0441\u0442\u0430\u0432\u043a\u0430\",\"id_func\":12,\"value\":25,\"write_enable\":1,\"val_min\":0,\"val_max\":40},{\"name\":\"\u0442\u0435\u043a\u0443\u0449\u0430\u044f \u0442\u0435\u043c\u043f\u0435\u0440\u0430\u0442\u0443\u0440\u0430\",\"id_func\":13,\"value\":21,\"write_enable\":0,\"val_min\":-40,\"val_max\":100}]}]},{\"name\":\"\u043a\u0443\u0445\u043d\u044f\",\"devices\":[{\"name\":\"\u041d\u0430\u0433\u0440\u0435\u0432\u0430\u0442\u0435\u043b\u044c\",\"id_dop_addr\":\"29\",\"functions\":[{\"name\":\"\u0432\u043a\u043b\\/\u0432\u044b\u043a\u043b \u0443\u0441\u0442\u0440\u043e\u0439\u0441\u0442\u0432\u043e\",\"id_func\":2,\"value\":0,\"write_enable\":1,\"val_min\":0,\"val_max\":1},{\"name\":\"\u0428\u0418\u041c\",\"id_func\":16,\"value\":11,\"write_enable\":1,\"val_min\":0,\"val_max\":100}]},{\"name\":\"\u0422\u0435\u0440\u043c\u043e\u043c\u0435\u0442\u0440\",\"id_dop_addr\":\"25\",\"functions\":[{\"name\":\"\u0432\u043a\u043b\\/\u0432\u044b\u043a\u043b \u0443\u0441\u0442\u0440\u043e\u0439\u0441\u0442\u0432\u043e\",\"id_func\":2,\"value\":1,\"write_enable\":1,\"val_min\":0,\"val_max\":1},{\"name\":\"\u0442\u0435\u0440\u043c\u043e\u0440\u0435\u0433\u0443\u043b\u044f\u0442\u043e\u0440_\u0443\u0441\u0442\u0430\u0432\u043a\u0430\",\"id_func\":12,\"value\":23,\"write_enable\":1,\"val_min\":0,\"val_max\":40},{\"name\":\"\u0442\u0435\u043a\u0443\u0449\u0430\u044f \u0442\u0435\u043c\u043f\u0435\u0440\u0430\u0442\u0443\u0440\u0430\",\"id_func\":13,\"value\":22,\"write_enable\":0,\"val_min\":-40,\"val_max\":100}]}]},{\"name\":\"\u0441\u043f\u0430\u043b\u044c\u043d\u044f\",\"devices\":[{\"name\":\"\u041d\u0430\u0433\u0440\u0435\u0432\u0430\u0442\u0435\u043b\u044c\",\"id_dop_addr\":\"28\",\"functions\":[{\"name\":\"\u0432\u043a\u043b\\/\u0432\u044b\u043a\u043b \u0443\u0441\u0442\u0440\u043e\u0439\u0441\u0442\u0432\u043e\",\"id_func\":2,\"value\":0,\"write_enable\":1,\"val_min\":0,\"val_max\":1},{\"name\":\"\u0428\u0418\u041c\",\"id_func\":16,\"value\":10,\"write_enable\":1,\"val_min\":0,\"val_max\":100}]},{\"name\":\"\u0422\u0435\u0440\u043c\u043e\u043c\u0435\u0442\u0440\",\"id_dop_addr\":\"24\",\"functions\":[{\"name\":\"\u0432\u043a\u043b\\/\u0432\u044b\u043a\u043b \u0443\u0441\u0442\u0440\u043e\u0439\u0441\u0442\u0432\u043e\",\"id_func\":2,\"value\":1,\"write_enable\":1,\"val_min\":0,\"val_max\":1},{\"name\":\"\u0442\u0435\u0440\u043c\u043e\u0440\u0435\u0433\u0443\u043b\u044f\u0442\u043e\u0440_\u0443\u0441\u0442\u0430\u0432\u043a\u0430\",\"id_func\":12,\"value\":53,\"write_enable\":1,\"val_min\":0,\"val_max\":40},{\"name\":\"\u0442\u0435\u043a\u0443\u0449\u0430\u044f \u0442\u0435\u043c\u043f\u0435\u0440\u0430\u0442\u0443\u0440\u0430\",\"id_func\":13,\"value\":23,\"write_enable\":0,\"val_min\":-40,\"val_max\":100}]}]}]}]}";
    private SharedPreferences sp;
    private SocketTask socketTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mLvDrawerMenu = (ListView) findViewById(R.id.lv_drawer_menu);

//        List<DrawerMenuItem> menuItems = generateDrawerMenuItems();
//        mDrawerMenuAdapter = new DrawerMenuItemAdapter(getApplicationContext(), menuItems);
//        mLvDrawerMenu.setAdapter(mDrawerMenuAdapter);

        mLvDrawerMenu.setOnItemClickListener(this);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        //---- FloatingActionButtons ------
        FloatingActionButton getRoomInfo = (FloatingActionButton) findViewById(R.id.getRoomInfo);
        FloatingActionButton sendRoomInfo = (FloatingActionButton) findViewById(R.id.connToSocket);
        getRoomInfo.setOnClickListener(this);
        sendRoomInfo.setOnClickListener(this);

        // получаем SharedPreferences, которое работает с файлом настроек
        sp = PreferenceManager.getDefaultSharedPreferences(this);
        // полная очистка настроек
        // sp.edit().clear().commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.getRoomInfo:
                getRoomInfo();
                break;
            case R.id.connToSocket:
                connToSocket();
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        //return super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
//            case R.id.action_settings:
//                Intent intent = new Intent(this, PrefActivity.class);
////                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
//                break;
            case R.id.action_logout:
                logoutUser();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    // выход пользователя из системы
    private void logoutUser() {
        // Session manager
        SessionManager session = new SessionManager(getApplicationContext());
        session.setLogin(false);

        // Если сокет-соединение было устрановлено, закрываем его
        if (socketTask != null) {
            socketTask.cancel(true);
        }

        // Launching the login activity
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    // Callback, срабатывающий при получении данных через сокет
    @Override
    public void onTaskDone() {
//        Toast.makeText(this,"Success",Toast.LENGTH_SHORT).show();

        // В ссответствии с полученными данными обновляем HomeInfo
        for(Category cat_next : HomeInfo.getCategories()){
            for(Room room_next : cat_next.getRooms()){
                for(Device dev_next : room_next.getDevices()){
                    for(State state_next : DeviceInfo.getStates()){
                        // Если устройства совпадают
                        if(dev_next.getIdDopAddr() == state_next.getIdDopAddr()){
                            for(Function func_next : dev_next.getFunctions()) {
                                // Если функции устройств совпадают
                                if(func_next.getIdFunc() == state_next.getIdFunc()){
                                    // Записываем новое значение
                                    func_next.setValue(state_next.getValue());
                                }
                            }
                        }
                    }
                }
            }
        }
        // Обновляем интерфейс
        updateInterface();
    }

    // установка сокет-соединения
    public void connToSocket(){
        if(demo){
            Toast.makeText(this, getResources().getString(R.string.not_socket_conn),Toast.LENGTH_SHORT).show();
        }
        else{
            if(socketTask != null) {
                socketTask.cancel(true);
            }
            Snackbar.make(this.findViewById(R.id.frame_container), getResources().getString(R.string.opening_socket), Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
            socketTask = new SocketTask(getApplicationContext(), this, address, 8000);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                socketTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else
                socketTask.execute();
        }
    }

    public void getRoomInfo(){
        if(demo){
            Snackbar.make(this.findViewById(R.id.frame_container), getResources().getString(R.string.local_loading), Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
            // convert from json
            JSON.parseObject(jsonDemo, HomeInfo.class);
            // create UI
            List<DrawerMenuItem> menuItems = generateDrawerMenuItems();
            mDrawerMenuAdapter = new DrawerMenuItemAdapter(getApplicationContext(), menuItems);
            mLvDrawerMenu.setAdapter(mDrawerMenuAdapter);
            Toast.makeText(this, getResources().getString(R.string.data_is_load),Toast.LENGTH_SHORT).show();
        }
        else{
            receiveRoomInfo();
        }
    }

    public void setRoomInfo(String[] keys, String[] values){
        if(demo){
            Toast.makeText(this, getResources().getString(R.string.not_change),Toast.LENGTH_SHORT).show();
        }
        else{
            sendRoomInfo(keys, values);
        }
    }

    // выполнение запроса конфигурации
    public void receiveRoomInfo(){
        boolean isNetworkAvailable = Utils.isNetworkAvailable(getApplicationContext());
        if (!isNetworkAvailable) {
            Snackbar.make(this.findViewById(R.id.frame_container), getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } else {
            GetTask task = new GetTask(this);
            Response res = null;
            try {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                    res = task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL1).get();
                else
                    res = task.execute(URL1).get();
            } catch (InterruptedException e) {
            } catch (ExecutionException e) {}

            if(res.status){
                jsonString = res.textResponse;
                // convert from json
                JSON.parseObject(jsonString, HomeInfo.class);
                // create interface
                List<DrawerMenuItem> menuItems = generateDrawerMenuItems();
                mDrawerMenuAdapter = new DrawerMenuItemAdapter(getApplicationContext(), menuItems);
                mLvDrawerMenu.setAdapter(mDrawerMenuAdapter);

                // Обновление интерфейса
                updateInterface();
            }
        }
    }

    // выполнение запроса на изменение параметров устройства
    public void sendRoomInfo(String[] keys, String[] values){
        boolean isNetworkAvailable = Utils.isNetworkAvailable(getApplicationContext());
        if (!isNetworkAvailable) {
            Snackbar.make(this.findViewById(R.id.frame_container), getResources().getString(R.string.no_connection), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } else {
            PostTask task = new PostTask(this, keys, values);
            Response res = null;
            try {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                    res = task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URL2).get();
                else
                    res = task.execute(URL2).get();
            } catch (InterruptedException e) {
            } catch (ExecutionException e) {}

            if(res.status){
                //для отладки
                if(debug) {
                    Toast.makeText(this,getResources().getString(R.string.answer) + res.textResponse,Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        onMenuSelected(position);
    }

    // Callback, срабатывающий при возобновлении работы Activity
    protected void onResume() {
//        demo = sp.getBoolean("demo", false);
        debug = sp.getBoolean("debug", false);
        withoutСonfirmation = sp.getBoolean("withoutСonfirmation", false);
        address = sp.getString("address", "");
//        URL1 = "http://" + address + AppConfig.URL_GET_CONFIG;
        URL1 = AppConfig.PROTOCOL + address + AppConfig.URL_GET_CONFIG;
        URL2 = AppConfig.PROTOCOL + address + AppConfig.URL_SET_VALUE;

        // Обновление интерфейса
        updateInterface();

        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(mLvDrawerMenu)) {
            mDrawerLayout.closeDrawer(mLvDrawerMenu);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

//    public void setFragment(int position, Class<? extends Fragment> fragmentClass) {
//        try {
//            Fragment fragment = fragmentClass.newInstance();
//            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
//            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentTransaction.replace(R.id.frame_container, fragment, fragmentClass.getSimpleName());
//            fragmentTransaction.commit();
//
//            mLvDrawerMenu.setItemChecked(position, true);
//            mDrawerLayout.closeDrawer(mLvDrawerMenu);
//            mLvDrawerMenu.invalidateViews();
//        }
//        catch (Exception ex){
//            Log.e("setFragment", ex.getMessage());
//        }
//    }


    // создание пунктов в меню навигации по результатам запроса конфигурации
    private List<DrawerMenuItem> generateDrawerMenuItems() {
        List<DrawerMenuItem> result = new ArrayList<DrawerMenuItem>();
        for(Category next : HomeInfo.getCategories()){
            DrawerMenuItem item = new DrawerMenuItem();
            item.setText(next.getName());
            result.add(item);
        }
        return result;
    }

    // обновление интерфейса
    public void updateInterface() {
        try{
            MenuFragment newMenuFragment = (MenuFragment) getSupportFragmentManager().findFragmentById(R.id.frame_container);

            if (newMenuFragment != null) {
                newMenuFragment.updateMenuView(mCurrentPosition);
//                Toast.makeText(this,"update "+mCurrentPosition, Toast.LENGTH_SHORT).show();
                // Загрузка фрагмента
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, newMenuFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }

            mLvDrawerMenu.setItemChecked(mCurrentPosition, true);
            mDrawerLayout.closeDrawer(mLvDrawerMenu);
            mLvDrawerMenu.invalidateViews();
        }
        catch (Throwable t) {
            // Ошибка при обновлении интерфейса фрагментов
//            Toast.makeText(this,"Up Error: "+t.toString(), Toast.LENGTH_LONG).show();
        }
    }

    // Callback, срабатывающий при выборе пункта в меню навигации
    public void onMenuSelected(int position) {
        try{
        getSupportActionBar().setTitle(HomeInfo.getCategories().get(position).getName());  // Title

        MenuFragment newMenuFragment = (MenuFragment) getSupportFragmentManager().findFragmentById(R.id.frame_container);
//        MenuFragment newMenuFragment = (MenuFragment) getSupportFragmentManager().findFragmentByTag("menu_0");

        // Если фрагмент создан, по обновляем его
        if (newMenuFragment != null) {
            // Если выбранный и текущий пункты не совпадают
            if (mCurrentPosition != position){
                newMenuFragment.updateMenuView(position);
//                Toast.makeText(this,"updateMenuView "+mCurrentPositionTab, Toast.LENGTH_SHORT).show();
                // Загрузка фрагмента
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, newMenuFragment);
                transaction.addToBackStack(null);
                transaction.commit();

                mCurrentPosition = position;
            }
        } else {
            // Если фрагмент не создан, создаём
            newMenuFragment = new MenuFragment();
            Bundle args = new Bundle();
            args.putInt(MenuFragment.ARG_POSITION, position);
            newMenuFragment.setArguments(args);
            // Загрузка фрагмента
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_container, newMenuFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }

        mLvDrawerMenu.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mLvDrawerMenu);
        mLvDrawerMenu.invalidateViews();

        }
        catch (Throwable t) {
            Toast.makeText(this,"Main Error: "+t.toString(), Toast.LENGTH_LONG).show();
        }
    }
}