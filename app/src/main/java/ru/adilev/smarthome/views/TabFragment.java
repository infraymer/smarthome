package ru.adilev.smarthome.views;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import ru.adilev.smarthome.R;
import ru.adilev.smarthome.Utils;
import ru.adilev.smarthome.entities.Device;
import ru.adilev.smarthome.entities.Function;
import ru.adilev.smarthome.entities.HomeInfo;

public class TabFragment extends Fragment {
    final static String ARG_POSITION = "position";
    private int mCurrentPosition = -1;
    final static String ARG_POSITION_TAB = "position_tab";
    private int mCurrentPositionTab = -1;

    private View rootView;
    private LinearLayout linLayout;
    private LayoutInflater ltInflater;

    private int mColor = 0;

    public TabFragment() {
        // Required empty public constructor

//        MainActivity.setCaller(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mCurrentPosition = getArguments().getInt(ARG_POSITION);
        mCurrentPositionTab = getArguments().getInt(ARG_POSITION_TAB);
        rootView = inflater.inflate(R.layout.tab_view, container, false);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        Bundle args = getArguments();
        if (args != null) {
            mCurrentPosition = args.getInt(ARG_POSITION);
            mCurrentPositionTab = args.getInt(ARG_POSITION_TAB);
        }
        updateTabView();
//        if (args != null) {
//            // Set fragment based on argument passed in
//            updateTabView(args.getInt(ARG_POSITION),args.getInt(ARG_POSITION_TAB));
//        } else if ((mCurrentPosition != -1) && mCurrentPositionTab != -1) {
//            // Set fragment based on saved instance state defined during onCreateView
//            updateTabView(mCurrentPosition, mCurrentPositionTab);
//        }
    }

//    @Override
//    public void onMainDone() {
//
//    }

    private void updateTabView() {
        try{
//        Toast.makeText(getActivity(),"Fragment's up " + mCurrentPositionTab,Toast.LENGTH_SHORT).show();
        // Отрисовываем список устройств
        linLayout = (LinearLayout) rootView.findViewById(R.id.linLayout);
        ltInflater = getActivity().getLayoutInflater();
        linLayout.removeAllViews();

        for(final Device dev_next : HomeInfo.getCategories().get(mCurrentPosition).getRooms().get(mCurrentPositionTab).getDevices()){
            //Связываемся с элементами разметки
            View item = ltInflater.inflate(R.layout.item_name, linLayout, false);
            TextView name = (TextView) item.findViewById(R.id.name);

            //Заполняем элементы
            name.setText(dev_next.getName());
            linLayout.addView(item);

            for(final Function func_next : dev_next.getFunctions()) {
                if( (func_next.getValMax() - func_next.getValMin()) == 1 ){
                    View funcItem = ltInflater.inflate(R.layout.item_func1, linLayout, false);
                    TextView funcName = (TextView) funcItem.findViewById(R.id.funcName);
                    funcName.setText(func_next.getName());

                    final Switch mSwitch = (Switch) funcItem.findViewById(R.id.sw);
                    mSwitch.setChecked( (func_next.getValue() != func_next.getValMin()) ); //convert int to bool
                    mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            final String mKeys[] = {"id_dop_addr","id_func","value"},
                                    mValues[] = {String.valueOf(dev_next.getIdDopAddr()),
                                            String.valueOf(func_next.getIdFunc()),
                                            String.valueOf((mSwitch.isChecked()) ? func_next.getValMax()  : func_next.getValMin())};
                            //для отладки
                            if(MainActivity.debug){
                                Toast.makeText(getActivity(),mKeys[0]+' '+mValues[0]+'\n'+
                                        mKeys[1]+' '+mValues[1]+'\n'+
                                        mKeys[2]+' '+mValues[2], Toast.LENGTH_SHORT).show();
//                                Toast.makeText(getActivity(),"id_dop_addr "+String.valueOf(dev_next.getIdDopAddr())+
//                                "\nid_func "+String.valueOf(func_next.getIdFunc())+
//                                "\nvalue "+String.valueOf((mSwitch.isChecked()) ? func_next.getValMax() : func_next.getValMin()), Toast.LENGTH_SHORT).show();
                            }
                            if(MainActivity.withoutСonfirmation){
                                ((MainActivity)getActivity()).setRoomInfo(mKeys, mValues);
                            }
                            else {
                                Snackbar.make(getView(), "Отправить данные?", Snackbar.LENGTH_LONG)
                                        .setAction("Ok", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                ((MainActivity)getActivity()).setRoomInfo(mKeys, mValues);
                                            }
                                        })
                                        .show();
                            }
                        }
                    });
                    linLayout.addView(funcItem);
                }
                else if(func_next.getIdFunc() == 11){
                    View colorItem = ltInflater.inflate(R.layout.item_color, linLayout, false);
                    Button btnColor = (Button) colorItem.findViewById(R.id.setColor);
                    btnColor.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Shows Color Picker dialog fragment. If color wasn't set previously, use BLUE by default
                            int thisColor = mColor == 0 ? Color.BLUE : mColor;
                            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                            ColorPickerDialogFragment fragment = ColorPickerDialogFragment.newInstance(thisColor);
                            fragment.setOnColorPickerListener(new OnColorPickerListener() {
                                @Override
                                public void onCancel(ColorPickerDialogFragment dialogFragment) {}
                                @Override
                                public void onOk(ColorPickerDialogFragment dialogFragment, final int color) {
                                    mColor = color;

                                    int red = Color.red(color);
                                    int green = Color.green(color);
                                    int blue = Color.blue(color);

                                    String strRGB = "RGB: ".concat(Integer.toString(red, 10)).
                                            concat(", ").concat(Integer.toString(green, 10)).
                                            concat(", ").concat(Integer.toString(blue, 10));

                                    String strHex = "HEX: #" + Utils.beautyHexString(Integer.toHexString(red)) +
                                            Utils.beautyHexString(Integer.toHexString(green)) +
                                            Utils.beautyHexString(Integer.toHexString(blue));

//                                    Toast.makeText(getActivity(), "onOk().\n"+String.valueOf(color)+'\n'+
//                                            strRGB+'\n'+strHex, Toast.LENGTH_SHORT).show();

                                    final String mKeys[] = {"id_dop_addr","id_func","value"},
                                            mValues[] = {String.valueOf(dev_next.getIdDopAddr()),
                                                    String.valueOf(func_next.getIdFunc()),
                                                    String.valueOf(color)};
                                    //для отладки
                                    if(MainActivity.debug){
                                        Toast.makeText(getActivity(),mKeys[0]+' '+mValues[0]+'\n'+
                                                mKeys[1]+' '+mValues[1]+'\n'+
                                                mKeys[2]+' '+mValues[2], Toast.LENGTH_SHORT).show();
                                    }
                                    if(MainActivity.withoutСonfirmation){
                                        ((MainActivity)getActivity()).setRoomInfo(mKeys, mValues);
                                    }
                                    else {
                                        Snackbar.make(getView(), "Отправить данные?", Snackbar.LENGTH_LONG)
                                                .setAction("Ok", new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        ((MainActivity)getActivity()).setRoomInfo(mKeys, mValues);
                                                    }
                                                })
                                                .show();
                                    }
                                }
                            });

                            fragment.show(ft, "color_picker_dialog");
                        }
                    });
                    linLayout.addView(colorItem);
                }
                else{
                    View funcItem = ltInflater.inflate(R.layout.item_func2, linLayout, false);
                    TextView funcName = (TextView) funcItem.findViewById(R.id.funcName);
                    TextView funcValue = (TextView) funcItem.findViewById(R.id.funcValue);
                    final TextView mCurrentText = (TextView) funcItem.findViewById(R.id.currentValue);
                    final SeekBar mSlider = (SeekBar) funcItem.findViewById(R.id.seekBar);

                    funcName.setText(func_next.getName());
                    //(next2.getValMax() != 1) &&
                    if(func_next.getWriteEnable() == 1){
                        funcValue.setVisibility(View.INVISIBLE);
                        //------------------------------
                        mCurrentText.setVisibility(View.VISIBLE);
                        mCurrentText.setText(String.valueOf(func_next.getValue()));
                        //------------------------------
                        mSlider.setVisibility(View.VISIBLE);
                        mSlider.setMax(func_next.getValMax());
                        mSlider.setProgress(func_next.getValue());
                    }
                    else{
                        funcValue.setText(String.valueOf(func_next.getValue()));
                    }
                    mSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            mCurrentText.setText(String.valueOf(progress));
                            final LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mCurrentText.getLayoutParams();
                            final int seekLocation = seekBar.getProgress() * (seekBar.getWidth() / seekBar.getMax());
//                            lp.leftMargin = seekLocation + seekBar.getPaddingLeft() + (int)seekBar.getX();
                            lp.leftMargin = seekLocation + (int)seekBar.getX();
                            mCurrentText.setLayoutParams(lp);

                            final String mKeys[] = {"id_dop_addr","id_func","value"},
                                    mValues[] = {String.valueOf(dev_next.getIdDopAddr()),
                                            String.valueOf(func_next.getIdFunc()),
                                            String.valueOf(mSlider.getProgress())};
                            //для отладки
                            if(MainActivity.debug){
                                Toast.makeText(getActivity(),mKeys[0]+' '+mValues[0]+'\n'+
                                        mKeys[1]+' '+mValues[1]+'\n'+
                                        mKeys[2]+' '+mValues[2], Toast.LENGTH_SHORT).show();
                            }
                            if(MainActivity.withoutСonfirmation){
                                ((MainActivity)getActivity()).setRoomInfo(mKeys, mValues);
                            }
                            else {
                                Snackbar.make(getView(), "Отправить данные?", Snackbar.LENGTH_LONG)
                                        .setAction("Ok", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                ((MainActivity)getActivity()).setRoomInfo(mKeys, mValues);
                                            }
                                        })
                                        .show();
                            }
                        }
                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {}
                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {}
                    });

                    linLayout.addView(funcItem);
                }
            }
            // Shows analytic
            View analyticItem = ltInflater.inflate(R.layout.item_analytic, linLayout, false);
            Button btnAnalytic = (Button) analyticItem.findViewById(R.id.showAnalytic);
            btnAnalytic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Shows Analytic Activity
                    Intent intent = new Intent(getActivity(), AnalyticActivity.class);
                    intent.putExtra("id_dop_addr", dev_next.getIdDopAddr());
                    intent.putExtra("mCurrentPosition", mCurrentPosition);
                    intent.putExtra("mCurrentPositionTab", mCurrentPositionTab);
                    startActivity(intent);
                }
            });
            linLayout.addView(analyticItem);
        }
        }
        catch (Throwable t) {
            Toast.makeText(getActivity(),"Error up tab: "+t.toString(), Toast.LENGTH_SHORT).show();
        }
        //-----------------------
//        mCurrentPosition = position;
//        mCurrentPositionTab = positionTab;
    }

//=======================================================================

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the current menu selection in case we need to recreate the fragment
        outState.putInt(ARG_POSITION, mCurrentPosition);
        outState.putInt(ARG_POSITION_TAB, mCurrentPositionTab);
    }
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//        if (savedInstanceState != null) {
//            //Restore the fragment's state here
//        }
//    }
//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//
//        //Save the fragment's state here
//    }

//    public static TabFragment newInstance(int pos, int posTab) {
//
//        TabFragment f = new TabFragment();
//        Bundle b = new Bundle();
//        b.putInt(ARG_POSITION, pos);
//        b.putInt(ARG_POSITION_TAB, posTab);
//
//        f.setArguments(b);
//        return f;
//    }
}